'use strict'

const $ = function(foo) {

    return document.getElementById(foo);
}

let ptstr = $("plaintext").value;
let cstr = $("cipher").value;
let output1 = $("encrypted");
let output2 = $("decrypted");

let abc = ["a", "b","c","d","e","f","g","h",
"i","j","k","l","m","n","o","p","q","r","s",
"t","u","v","x","y","z","æ","ø","å"]

function encrypt () {
let newstr = "";
let input = ptstr.toLowerCase();

	for (let i = 0; i < input.length; i++) {
	let c = input[i];		
	let code = abc.indexOf(c);
		
	if (code < 25){	
		newstr = newstr.concat(abc[code + 3]);
		} else if (code >= 25) {
		newstr = newstr.concat(abc[code - 25]);
		}

	}

	output1.innerHTML = newstr;

}

function decrypt () {
let newstr = "";
let input = cstr.toLowerCase();

	for (let i = 0; i < input.length; i++) {
	let c = input[i];		
	let code = abc.indexOf(c);
		
	if (code < 25){	
		newstr = newstr.concat(abc[code - 3]);
		} else if (code >= 25) {
		newstr = newstr.concat(abc[code + 25]);
		}

	}

	output2.innerHTML = newstr;

}






$("decrypt").addEventListener("click", decrypt);

$("encrypt").addEventListener("click", encrypt);

