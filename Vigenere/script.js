'use strict'

const $ = function(foo) {

    return document.getElementById(foo);
}

let ptstr = $("plaintext").value;
let cstr = $("cipher").value;
let output1 = $("encrypted");
let output2 = $("decrypted");

let keyval = $("key").value;

let abc = ["a", "b","c","d","e","f","g","h",
"i","j","k","l","m","n","o","p","q","r","s",
"t","u","v","x","y","z","æ","ø","å"]

//const abc = {1: "a", 2: "b", 3: "c", 4: "d", 5: "e", 
//6: "f", 7: "g", 8: "h", 9: "i", 10: "j", 11: "k", 12: "l", 
//13: "m", 14: "n", 15: "o", 16: "p", 17: "q", 18: "r", 
//19: "s", 20: "t", 21: "u", 22: "v", 23: "w", 24: "x", 
//25: "y", 26: "z", 27: "æ", 28: "ø", 29: "å"
//}




function encrypt () {
let j = 0;
let newstr = "";
let input = ptstr.toLowerCase();
let key = keyval.toLowerCase();
let keycode;

	for (let i = 0; i < input.length; i++) {

		let c = input[i];		
		let code = abc.indexOf(c);

		let k = key[j];
		keycode = abc.indexOf(k);
			console.log(j, keycode, k);
		if (code < 28 - keycode){	
			newstr = newstr.concat(abc[code + keycode]);
			} else if (code >= 28 - keycode) {

			newstr = newstr.concat(abc[code - (28 - keycode)]);
		}


		output1.innerHTML = newstr;

		if (j >= (key.length - 1)) {
			j = 0;
			} else { 
			j++;
		}
	
	}

}


function decrypt () {
let j = 0;
let newstr = "";
let input = ptstr.toLowerCase();
let key = keyval.toLowerCase();
let keycode;

	for (let i = 0; i < input.length; i++) {

		let c = input[i];		
		let code = abc.indexOf(c);

		let k = key[j];
		keycode = abc.indexOf(k);
			console.log(j, keycode, k);
		if (code < 28 + keycode){	
			newstr = newstr.concat(abc[code - keycode]);
			} else if (code >= 28 - keycode) {

			newstr = newstr.concat(abc[code + (28 - keycode)]);
		}


		output2.innerHTML = newstr;

		if (j >= (key.length - 1)) {
			j = 0;
			} else { 
			j++;
		}
	
	}

}






$("decrypt").addEventListener("click", decrypt);

$("encrypt").addEventListener("click", encrypt);

